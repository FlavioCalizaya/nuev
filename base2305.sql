drop trigger tgrInsertEstablecimiento

DELIMITER $$
CREATE TRIGGER tgrInsertEstablecimiento BEFORE INSERT ON establecimiento 
FOR EACH ROW
BEGIN
  DECLARE _cantidad INT;
  DECLARE _ci VARCHAR(15);
  DECLARE _nombre VARCHAR(160);
  DECLARE _municipio VARCHAR(160);
  IF EXISTS (SELECT idPersona FROM persona WHERE idPersona=NEW.idPersona) THEN
    
		  SELECT nombreMunicipio INTO _municipio FROM municipio WHERE idMunicipio=NEW.idMunicipio;

          IF EXISTS (SELECT idPersona FROM personaempresa WHERE idPersona=NEW.idPersona) THEN
            -- UPDATE
            UPDATE personaempresa SET cantidadEstablecimientos=cantidadEstablecimientos+1, penultimoEstablecimiento=ultimoEstablecimiento,
                    ultimoEstablecimiento=CONCAT(NEW.razonSocial,' -- ',_municipio), fechaActualizacion=CURRENT_TIMESTAMP
            WHERE idPersona=NEW.idPersona;
		  ELSE
             -- INSERT
             SELECT ci, CONCAT(primerApellido,' ',segundoApellido,' ',nombres) INTO _ci, _nombre FROM persona WHERE idPersona=NEW.idPersona;
			 SELECT COUNT(*) INTO _cantidad FROM establecimiento WHERE idPersona=NEW.idPersona;             
             INSERT INTO personaempresa (idPersona, ci, nombreCompleto, cantidadEstablecimientos, ultimoEstablecimiento, fechaActualizacion)
			 VALUES (NEW.idPersona, _ci, _nombre, _cantidad, CONCAT(NEW.razonSocial,' -- ',_municipio), CURRENT_TIMESTAMP);
          END IF;			

    END IF;

END$$